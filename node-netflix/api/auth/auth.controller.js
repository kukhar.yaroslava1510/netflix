const authService = require("./auth.service");
const User = require("../models/User");
const CryptoJS = require("crypto-js");
const jwt = require("jsonwebtoken");
module.exports = {
    async register (req, res) {
        try {
            const user = await authService.register({
                username: req.body.username,
                email: req.body.email,
                password: req.body.password,
            });
            res.status(201).json(user);
        } catch (err) {
            res.status(500).json(err);
        }
    },
    async login (req, res) {
        try {
            const user = await authService.login ({ email: req.body.email });
            const bytes = CryptoJS.AES.decrypt(user.password, process.env.SECRET_KEY);
            const originalPassword = bytes.toString(CryptoJS.enc.Utf8);

            res.status(200).json({ ...info, accessToken });
        } catch (err) {
            res.status(500).json(err);
        }
    }
}