const User = require("../models/User");
const CryptoJS = require("crypto-js");
module.exports = {
    async register (userDto) {
        const newUser = new User({
            username: userDto.username,
            email: userDto.email,
            password: CryptoJS.AES.encrypt(
                userDto.password,
                process.env.SECRET_KEY
            ).toString(),
        });
        return newUser.save();
    },
    async login (userDto) {
        const User =  new User ({
            username: userDto.username,
            email: userDto.email,
            password: CryptoJS.AES.encrypt(
                userDto.password,
                process.env.SECRET_KEY).findOne(userDto.email, userDto.password)
        });


        return User;
}